package com.misiontic.post_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class postMS {

	public static void main(String[] args) {
		SpringApplication.run(postMS.class, args);
	}

}
