package com.controllers;
import com.models.comments;
import com.repositories.commentsRepository;
import org.springframework.web.bind.annotation.*;
@RestController
public class commentsController {
    private final commentsRepository commentsRepository;
    public commentsController(commentsRepository commentsRepository) {
        this.commentsRepository = commentsRepository;
    }
    @GetMapping("/comments/{userName}")
    comments getComments(@PathVariable String userName){
        return commentsRepository.findById(userName).orElseThrow();
    }
    @PostMapping("/comments")
    comments newComment(@RequestBody comments comments){
        return commentsRepository.save(comments);
    }
}