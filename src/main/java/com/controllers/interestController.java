package com.controllers;
import com.exceptions.interestException;
import com.models.interest;
import com.repositories.interestRepository;
import org.springframework.web.bind.annotation.*;
@RestController
public class interestController {
    private final interestRepository interestRepository;
    public interestController(interestRepository interestRepository) {
        this.interestRepository = interestRepository;
    }
    @GetMapping("/interest/{userName}")
    interest getInterest(@PathVariable String userName){
        return interestRepository.findById(userName).orElseThrow(() -> new interestException("No se encontro una cuenta con el username: " + userName));
    }
    @PostMapping("/interest")
    interest newInterest(@RequestBody interest Interest){
        return interestRepository.save(Interest);
    }
}